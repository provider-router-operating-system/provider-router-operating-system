#!/bin/bash

acceptSSHRoot(){
	echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
	service ssh restart
}

installPackages() {
	local packages=(vim tcpdump ppp xl2tpd kea frr frr-pythontools)

	#Install FRR
	curl -s https://deb.frrouting.org/frr/keys.gpg | tee /usr/share/keyrings/frrouting.gpg > /dev/null
	FRRVER="frr-stable"
	echo deb '[signed-by=/usr/share/keyrings/frrouting.gpg]' https://deb.frrouting.org/frr $(lsb_release -s -c) $FRRVER | tee -a /etc/apt/sources.list.d/frr.list

	apt update
	apt install -y ${packages[@]}
}

activateRouting(){
	echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/60-routeros.conf
	sysctl -p /etc/sysctl.d/60-routeros.conf
}

installRouterOS() {
	apt install curl -y
	acceptSSHRoot
}

case $1 in
	"install")
		installRouterOS
		installPackages
		activateRouting
	;;

esac
